package org.nishi.bigNumber.hack;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.zip.CRC32;

public class Keygen {
    /**
     * @param s
     * @param i
     * @param bytes
     * @return
     */
    public static short getCRC(String s, int i, byte bytes[]) {
        CRC32 crc32 = new CRC32();
        if (s != null) {
            for (int j = 0; j < s.length(); j++) {
                char c = s.charAt(j);
                crc32.update(c);
            }
        }
        crc32.update(i);
        crc32.update(i >> 8);
        crc32.update(i >> 16);
        crc32.update(i >> 24);
        for (int k = 0; k < bytes.length - 2; k++) {
            byte byte0 = bytes[k];
            crc32.update(byte0);
        }
        return (short) (int) crc32.getValue();
    }

    /**
     * @param i
     * @return
     */
    public static String encodeGroup(int i) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < 5; j++) {
            int k = i % 36;
            char c;
            if (k < 10) {
                c = (char) (48 + k);
            } else {
                c = (char) ((65 + k) - 10);
            }
            sb.append(c);
            i /= 36;
        }
        return sb.toString();
    }

    /**
     * @param biginteger
     * @return String
     */
    public static String encodeGroups(BigInteger biginteger) {
        BigInteger beginner1 = BigInteger.valueOf(0x39aa400L);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; biginteger.compareTo(BigInteger.ZERO) != 0; i++) {
            int j = biginteger.mod(beginner1).intValue();
            String s1 = encodeGroup(j);
            if (i > 0) {
                sb.append("-");
            }
            sb.append(s1);
            biginteger = biginteger.divide(beginner1);
        }
        return sb.toString();
    }

    /**
     * @param name    Name of the license owner
     * @param days    Days of validation (Use 0 for lifetime validation)
     * @param version version of the intelliJ idea
     * @return product key to register intelliJ for the user having the provided <code>name</code>
     */
    public static String makeKey(String name, int days, int version) {
        int id = new Random().nextInt(100000);
        id %= 100000;
        byte[] byteKey = new byte[12];
        byteKey[0] = (byte) 1; // Product type: IntelliJ IDEA is 1
        byteKey[1] = (byte) version;
        Date d = new Date();
        long ld = (d.getTime() >> 16);
        byteKey[2] = (byte) (ld & 255);
        byteKey[3] = (byte) ((ld >> 8) & 255);
        byteKey[4] = (byte) ((ld >> 16) & 255);
        byteKey[5] = (byte) ((ld >> 24) & 255);
        days &= 0xffff;
        byteKey[6] = (byte) (days & 255);
        byteKey[7] = (byte) ((days >> 8) & 255);
        byteKey[8] = 105;
        byteKey[9] = -59;
        byteKey[10] = 0;
        byteKey[11] = 0;
        int intCRC = Keygen.getCRC(name, id % 100000, byteKey);
        byteKey[10] = (byte) (intCRC & 255);
        byteKey[11] = (byte) ((intCRC >> 8) & 255);
        BigInteger pow = new BigInteger("89126272330128007543578052027888001981", 10);
        BigInteger mod = new BigInteger("86f71688cdd2612ca117d1f54bdae029", 16);
        BigInteger bigIntegerKey = new BigInteger(byteKey);
        BigInteger k1 = bigIntegerKey.modPow(pow, mod);
        String productKey = Integer.toString(id);
        String sz = "0";
        while (productKey.length() != 5) {
            productKey = sz.concat(productKey);
        }
        productKey = productKey.concat("-");
        String s1 = Keygen.encodeGroups(k1);
        productKey = productKey.concat(s1);
        return productKey;
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.printf("Please provide your name followed by the major version of IntelliJ Idea, you want to activate.");
            return;
        }
        int version = Integer.parseInt(args[args.length - 1]);
        args[args.length - 1] = null;
        String name = ("" + Arrays.asList(args)).replaceAll("(^.|.$)", "").replace(", null", "").replace(", ", " ");
        System.out.println(name + ", Your product key for IntelliJ Idea " + version + " is " + Keygen.makeKey(name, 0, version) + ".");
    }
}
